package com.chirademo;

import android.app.Application;

import com.chirademo.model.MyObjectBox;

import io.objectbox.BoxStore;

public class AppClass extends Application {

    private BoxStore boxStore;

    @Override
    public void onCreate() {
        super.onCreate();
        boxStore = MyObjectBox.builder().androidContext(AppClass.this).build();
    }
    public BoxStore getBoxStore() {
        return boxStore;
    }
}
