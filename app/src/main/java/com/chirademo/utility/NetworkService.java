package com.chirademo.utility;

import com.chirademo.model.ResponseModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;



public interface NetworkService {

    @GET("v1/gifs/search?")
    Observable<ResponseModel> getSearchImage(@Query("q") String searchKeyword,
                                             @Query("api_key") String api_key,
                                             @Query("limit") int limit);
}
