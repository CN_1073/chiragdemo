package com.chirademo.view;

public interface VideoPlayView {

    void setCount(long upCount, long downCount);
    void showVideo(String url);
}
