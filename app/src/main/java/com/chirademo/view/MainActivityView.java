package com.chirademo.view;

import com.chirademo.adapter.ImageAdapter;

public interface MainActivityView {

    void searchValidationListner(String message);
    void noVideoSearch(String message);
    void setImageList(ImageAdapter adapter);
}
