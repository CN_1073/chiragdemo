package com.chirademo.presenter;

import android.content.Context;
import android.content.Intent;

import com.chirademo.Activity.VideoPlayActivity;
import com.chirademo.R;
import com.chirademo.adapter.ImageAdapter;
import com.chirademo.interactor.MainAactivityInteractor;
import com.chirademo.interactor.MainActivityInteractorIml;
import com.chirademo.model.VideoModel;
import com.chirademo.view.MainActivityView;

import java.util.ArrayList;

public class MainActivityPresenterIml implements MainAactivityInteractor {

    private Context mContext;
    private MainActivityView viewListner;
    private MainActivityInteractorIml interactorIml;
    private ImageAdapter adapter;
    private ArrayList<VideoModel> videolist;

    public MainActivityPresenterIml(Context context, MainActivityView view) {
        this.mContext = context;
        this.viewListner = view;
        interactorIml = new MainActivityInteractorIml(mContext, this);
    }

    public void searchVideoClick(String searchValue) {

        if (searchValue == null || searchValue.isEmpty()) {
            viewListner.searchValidationListner(mContext.getString(R.string.entersearchkeyword));
        } else {
            interactorIml.getVideoSearchList(searchValue, 10);
        }
    }

    public void navigateToVideoPlay(int position) {

        Intent intent = new Intent(mContext, VideoPlayActivity.class);
        intent.putExtra("VideoDetail", videolist.get(position));
        mContext.startActivity(intent);

    }

    @Override
    public void searchVideoResult(ArrayList<VideoModel> videoModelArrayList) {
        adapter = new ImageAdapter(mContext, videoModelArrayList);
        viewListner.setImageList(adapter);
        videolist = videoModelArrayList;
    }

    @Override
    public void noSearchVideo() {
        viewListner.noVideoSearch(mContext.getString(R.string.novideo));
    }

    @Override
    public void connectionError(String message) {
        viewListner.noVideoSearch(message);
    }
}
