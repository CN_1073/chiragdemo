package com.chirademo.presenter;

import android.app.Activity;
import android.util.Log;

import com.chirademo.interactor.VideoPlayInteractor;
import com.chirademo.interactor.VideoPlayInteractorImpl;
import com.chirademo.model.ObjectModel;
import com.chirademo.model.VideoModel;
import com.chirademo.view.VideoPlayView;

public class VideoPlayPresenterImpl implements VideoPlayInteractor {

    private final String MODULE = "VideoPlayPresenterImpl";
    private Activity mContext;
    private VideoPlayView viewListner;
    private VideoModel videoDetail;
    private VideoPlayInteractorImpl interactor;
    private long upCount, downCount;

    public VideoPlayPresenterImpl(Activity context, VideoPlayView view) {
        this.mContext = context;
        this.viewListner = view;
        interactor = new VideoPlayInteractorImpl(mContext, this);

        videoDetail = context.getIntent().getParcelableExtra("VideoDetail");
        interactor.getThumbCount(videoDetail.getId());

        viewListner.showVideo(videoDetail.getImages().getOriginal().getMp4());

        Log.d(MODULE, "Video Title - " + videoDetail.getId());
        Log.d(MODULE, "Video Title - " + videoDetail.getTitle());
        Log.d(MODULE, "Video Url - " + videoDetail.getImages().getOriginal().getMp4());
    }

    @Override
    public void getVideoDetail(ObjectModel model) {

        upCount = model.getUpCount();
        downCount = model.getDownCount();
        viewListner.setCount(upCount, downCount);
    }

    public void upCountIncrese() {

        Log.d(MODULE, upCount + "");
        upCount++;
        interactor.insertThumbCount(videoDetail.getId(), upCount, true);

    }

    public void downCountIncrease() {
        Log.d(MODULE, downCount + "");
        downCount++;
        interactor.insertThumbCount(videoDetail.getId(), downCount, false);
    }


    @Override
    public void videoCountUpdateListner() {
        viewListner.setCount(upCount, downCount);
    }
}
