package com.chirademo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.chirademo.R;
import com.chirademo.model.VideoModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImageAdapter extends BaseAdapter {

    private final String MODULE = "ImageAdapter";
    private Context mContext;
    private ArrayList<VideoModel> videoList;
    private LayoutInflater inflater;
    private int width;
    private int height;

    public ImageAdapter(Context context, ArrayList<VideoModel> videoModelArrayList) {
        this.mContext = context;
        this.videoList = videoModelArrayList;
        width = mContext.getResources().getDisplayMetrics().widthPixels / 2;
        height = width;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return videoList.size();
    }

    @Override
    public VideoModel.ImagesModel.OriginalModel getItem(int position) {
        return videoList.get(position).getImages().getOriginal();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageHolder holder;

        VideoModel.ImagesModel.OriginalModel imagePojo = getItem(position);

        if (convertView == null) {

            holder = new ImageHolder();
            convertView = inflater.inflate(R.layout.row_image_adapter, null);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width,height);
            holder.imageView = convertView.findViewById(R.id.iv_video_thumbnail);
            holder.imageView.setLayoutParams(params);

            convertView.setTag(holder);
        } else {
            holder = (ImageHolder) convertView.getTag();
        }
        Picasso.get()
                .load(imagePojo.getUrl())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.imageView);

        return convertView;
    }

    class ImageHolder {
        ImageView imageView;
    }
}
