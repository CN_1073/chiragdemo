package com.chirademo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.chirademo.R;
import com.chirademo.adapter.ImageAdapter;
import com.chirademo.presenter.MainActivityPresenterIml;
import com.chirademo.view.MainActivityView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity implements MainActivityView {

    private final String MODULE = "MainActivity";

    @BindView(R.id.tilSearchVideo)
    TextInputLayout tilSearchVideo;
    @BindView(R.id.edt_searchVideo)
    TextInputEditText edt_searchVideo;
    @BindView(R.id.btn_search)
    AppCompatButton btn_search;
    @BindView(R.id.lv_videoList)
    GridView lv_VideoList;
    @BindView(R.id.tv_novideo)
    TextView tv_novideo;

    private MainActivityPresenterIml presenterIml;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenterIml = new MainActivityPresenterIml(this, this);
        lv_VideoList.setOnItemClickListener(onItemClickListener);
    }

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            presenterIml.navigateToVideoPlay(position);
        }
    };


    @OnClick(R.id.btn_search)
    public void searchClick() {
        String searchValue = edt_searchVideo.getText().toString().trim();
        presenterIml.searchVideoClick(searchValue);
    }

    @Override
    public void searchValidationListner(String message) {
        Log.d(MODULE, "Search Keyword Empty");
        tilSearchVideo.setError(message);
    }

    @Override
    public void noVideoSearch(String message) {
        lv_VideoList.setVisibility(View.GONE);
        tv_novideo.setVisibility(View.VISIBLE);
        tv_novideo.setText(message);
    }

    @Override
    public void setImageList(ImageAdapter adapter) {
        lv_VideoList.setAdapter(adapter);
        lv_VideoList.setVisibility(View.VISIBLE);
        tv_novideo.setVisibility(View.GONE);
    }
}
