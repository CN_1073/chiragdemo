package com.chirademo.interactor;

import com.chirademo.model.ObjectModel;

public interface VideoPlayInteractor {

    void getVideoDetail(ObjectModel model);
    void videoCountUpdateListner();
}
