package com.chirademo.interactor;

import android.content.Context;
import android.util.Log;

import com.chirademo.BuildConfig;
import com.chirademo.model.ResponseModel;
import com.chirademo.utility.NetworkService;
import com.chirademo.utility.Service;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainActivityInteractorIml {

    private final String MODULE = "InteractorIml";
    private Context mContext;
    private MainAactivityInteractor interactorListner;
    private Retrofit retrofit;

    public MainActivityInteractorIml(Context context, MainAactivityInteractor interactor) {
        this.mContext = context;
        this.interactorListner = interactor;
        retrofit = new Service().getRetrofit();
    }

    public void getVideoSearchList(String searchKey, int limit) {

        retrofit.create(NetworkService.class).getSearchImage(searchKey, BuildConfig.APIKEY, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResults, this::handleError);
    }

    private void handleResults(ResponseModel responseModel) {

        if(responseModel.getMeta().getStatus() == 200) {

            Log.d(MODULE, responseModel.getData().size()+" Total Size");

            if(responseModel.getData().size() > 0) {
                interactorListner.searchVideoResult(responseModel.getData());
            } else {
                Log.d(MODULE,"Video not avilable");
                interactorListner.noSearchVideo();
            }

        }  else {
            interactorListner.connectionError(responseModel.getMeta().getMsg());
        }
    }

    private void handleError(Throwable t) {
        Log.d(MODULE,"Internet Connection Error "+t.getMessage());
        interactorListner.connectionError(t.getMessage());
    }
}
