package com.chirademo.interactor;

import com.chirademo.model.VideoModel;

import java.util.ArrayList;

public interface MainAactivityInteractor {

    void searchVideoResult(ArrayList<VideoModel> videoModelArrayList);

    void noSearchVideo();

    void connectionError(String message);
}
