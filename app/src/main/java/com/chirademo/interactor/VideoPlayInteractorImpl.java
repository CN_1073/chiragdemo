package com.chirademo.interactor;

import android.app.Activity;
import android.util.Log;

import com.chirademo.AppClass;
import com.chirademo.model.ObjectModel;
import com.chirademo.model.ObjectModel_;

import io.objectbox.Box;
import io.objectbox.BoxStore;

public class VideoPlayInteractorImpl {

    private final String MODULE = "VideoPlayInteractorImpl";
    private Activity mContext;
    private VideoPlayInteractor interactorListner;
    private BoxStore boxStore;
    private Box<ObjectModel> videoBox;

    public VideoPlayInteractorImpl(Activity context, VideoPlayInteractor interactor) {

        this.interactorListner = interactor;
        mContext = context;

        boxStore = ((AppClass) mContext.getApplication()).getBoxStore();
        videoBox = boxStore.boxFor(ObjectModel.class);
    }


    public void getThumbCount(String videoid) {

        ObjectModel videoDetail = videoBox.query().equal(ObjectModel_.video_id, videoid).build().findFirst();
        Log.d(MODULE, "get video Thumbsup Count");
        if (videoDetail != null) {
            interactorListner.getVideoDetail(videoDetail);
        }
    }

    public void insertThumbCount(String videoId, long count, boolean isUpCount) {

        ObjectModel videoDetail = videoBox.query().equal(ObjectModel_.video_id, videoId).build().findFirst();

        if (videoDetail == null) {

            ObjectModel model = new ObjectModel();
            model.setVideo_id(videoId);

            if (isUpCount) {
                model.setUpCount(count);
                model.setDownCount(0);
            } else {
                model.setUpCount(0);
                model.setDownCount(count);
            }
            videoBox.put(model);

        } else {
            if (isUpCount) {
                videoDetail.setUpCount(count);
            } else {
                videoDetail.setDownCount(count);
            }

            videoBox.put(videoDetail);
        }
        Log.d(MODULE, "Inster or update Thumbsup Count");
        interactorListner.videoCountUpdateListner();
    }
}
