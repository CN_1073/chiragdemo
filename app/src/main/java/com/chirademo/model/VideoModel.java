package com.chirademo.model;

import android.os.Parcel;
import android.os.Parcelable;

public class VideoModel implements Parcelable {


    private String id;
    private String title;
    private ImagesModel images;

    protected VideoModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        images = in.readParcelable(ImagesModel.class.getClassLoader());
    }

    public static final Creator<VideoModel> CREATOR = new Creator<VideoModel>() {
        @Override
        public VideoModel createFromParcel(Parcel in) {
            return new VideoModel(in);
        }

        @Override
        public VideoModel[] newArray(int size) {
            return new VideoModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ImagesModel getImages() {
        return images;
    }

    public void setImages(ImagesModel images) {
        this.images = images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeParcelable(images, flags);
    }


    public static class ImagesModel implements Parcelable {

        private OriginalModel original;

        public ImagesModel() {
        }

        protected ImagesModel(Parcel in) {
            original = in.readParcelable(OriginalModel.class.getClassLoader());
        }

        public static final Creator<ImagesModel> CREATOR = new Creator<ImagesModel>() {
            @Override
            public ImagesModel createFromParcel(Parcel in) {
                return new ImagesModel(in);
            }

            @Override
            public ImagesModel[] newArray(int size) {
                return new ImagesModel[size];
            }
        };

        public OriginalModel getOriginal() {
            return original;
        }

        public void setOriginal(OriginalModel original) {
            this.original = original;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(original, flags);
        }

        public static class OriginalModel implements Parcelable {

            private String url;
            private int width;
            private int height;
            private long size;
            private int frames;
            private String mp4;
            private long mp4_size;
            private String webp;
            private long webp_size;


            public OriginalModel() {
            }


            protected OriginalModel(Parcel in) {
                url = in.readString();
                width = in.readInt();
                height = in.readInt();
                size = in.readLong();
                frames = in.readInt();
                mp4 = in.readString();
                mp4_size = in.readLong();
                webp = in.readString();
                webp_size = in.readLong();
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(url);
                dest.writeInt(width);
                dest.writeInt(height);
                dest.writeLong(size);
                dest.writeInt(frames);
                dest.writeString(mp4);
                dest.writeLong(mp4_size);
                dest.writeString(webp);
                dest.writeLong(webp_size);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<OriginalModel> CREATOR = new Creator<OriginalModel>() {
                @Override
                public OriginalModel createFromParcel(Parcel in) {
                    return new OriginalModel(in);
                }

                @Override
                public OriginalModel[] newArray(int size) {
                    return new OriginalModel[size];
                }
            };

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public int getWidth() {
                return width;
            }

            public void setWidth(int width) {
                this.width = width;
            }

            public int getHeight() {
                return height;
            }

            public void setHeight(int height) {
                this.height = height;
            }

            public long getSize() {
                return size;
            }

            public void setSize(long size) {
                this.size = size;
            }

            public int getFrames() {
                return frames;
            }

            public void setFrames(int frames) {
                this.frames = frames;
            }

            public String getMp4() {
                return mp4;
            }

            public void setMp4(String mp4) {
                this.mp4 = mp4;
            }

            public long getMp4_size() {
                return mp4_size;
            }

            public void setMp4_size(long mp4_size) {
                this.mp4_size = mp4_size;
            }

            public String getWebp() {
                return webp;
            }

            public void setWebp(String webp) {
                this.webp = webp;
            }

            public long getWebp_size() {
                return webp_size;
            }

            public void setWebp_size(long webp_size) {
                this.webp_size = webp_size;
            }
        }
    }
}
