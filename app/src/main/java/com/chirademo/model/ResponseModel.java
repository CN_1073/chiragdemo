package com.chirademo.model;

import java.util.ArrayList;

public class ResponseModel {

    private PaginationModel pagination;
    private MetaModel meta;
    private ArrayList<VideoModel> data;

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }

    public MetaModel getMeta() {
        return meta;
    }

    public void setMeta(MetaModel meta) {
        this.meta = meta;
    }

    public ArrayList<VideoModel> getData() {
        return data;
    }

    public void setData(ArrayList<VideoModel> data) {
        this.data = data;
    }

    public class PaginationModel {

        private int total_count;
        private int count;
        private int offset;

        public int getTotal_count() {
            return total_count;
        }

        public void setTotal_count(int total_count) {
            this.total_count = total_count;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }
    }

    public class MetaModel {

        private int status;
        private String msg;
        private String response_id;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getResponse_id() {
            return response_id;
        }

        public void setResponse_id(String response_id) {
            this.response_id = response_id;
        }
    }
}
